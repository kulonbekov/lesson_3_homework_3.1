package com.company;


import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        String[] names = {"Улан", "Адилет", "Элдос",}; //Массив из типа String
        /*for (int i = 0; i < names.length; i++) {
            switch (i) {
                case 0:
                    System.out.println(names[i] + " доброе утро!");
                    break;
                case 1:
                    System.out.println(names[i] + " добрый день!");
                    break;
                case 2:
                    System.out.println(names[i] + " добрый вечер!");
                    break;
                default:
                    System.out.println("Значение массива не правильно");
                    break;*/


        names = Arrays.copyOf(names, names.length + 1);
        names[names.length - 1] = "Мирлан";


        for (int i = 0; i < names.length; i++) {
            switch (i) {
                case 0:
                    System.out.println(names[i] + " доброе утро!");
                    break;
                case 1:
                    System.out.println(names[i] + " добрый день!");
                    break;
                case 2:
                    System.out.println(names[i] + " добрый вечер!");
                    break;
                case 3:
                    System.out.println(names[i] + " доброй ночи!");
                    break;
                default:
                    System.out.println("Значение массива не правильно");
                    break;
            }
        }
    }
}
